# meti-VL-Info3-Beispielprogramme

**Beispielprogramme aus der Vorlesung, Übungen**

Die Programme sind jeweils in einem eigenen Ordner gespeichert. Das Repository wird im Laufe der Vorlesung fortlaufend aktualisiert. Kursteilnehmerinnen und  -teilnehmer können sich von diesem Repository einen Clone anlegen, der sich dann fortlaufend per pull-Operation aktualisieren lässt.

# Inhalt des Repositories

## Digitaltechnik
Es werden Beispiele für das Programm LogicCircuit in einem Ordner angeboten (im WS20/21 noch nicht im Repo vorhanden; stattdessen: RELAX)
## Java
Die in der VL gezeigten Java Programme werden in einem eigenen Ordner hinterlegt.

# Änderungswünsche und Fehlermeldungen
Für die Kursteilnehmerinnen und -teilnehmer ist es nicht möglich eigene Änderungen in dieses Repository zu übertragen. Es können jedoch Änderungswünsche oder Fehlermeldungen über den GitLab Issue Tracker gemeldet werden. 

2024-08-01
