/** StringBufPerformance.java
* 
* Das Programm zeigt, dass die Verwendung der Klasse 
* StringBuffer im Vergleich zu String einen eindeutigen
* Vorteil bringt, wenn die L�nge des Strings h�ufig ver�ndert
* wird.
*
* @author Steddin
* @version 1.00, 2016-11-22
*/
package inf;
import java.time.*;

public class StringBufPerformance {

	public static void main(String[] args) {
		Instant startTime, stopTime;
		
		String testString = "blabla\n";
		startTime = Instant.now();
		for (int ii=0; ii< 10_000; ii++) {
			testString += "blabla\n";
		}
		stopTime = Instant.now();
		System.out.println("Dauer f�r String-Operation: "
							+ Duration.between(startTime, stopTime).toMillis()*1000 + "�s");

		StringBuffer testSB = new StringBuffer("blabla\n");
		// zu schnell ... StringBuffer testSB = new StringBuffer("blabla\n".length()*1000);
		startTime = Instant.now();
		for (int ii=0; ii< 1_000_000; ii++) {
			testSB.append("blabla\n");
		}
		stopTime = Instant.now();
		System.out.println("Dauer f�r StringBuffer-Operation: "
							+ (Duration.between(startTime, stopTime).toMillis()*10) + "�s");
		//Multiplikation statt mit 1000 nur mir 10: Dies entspricht dann der Zeit von 10000 Durchl�ufen
	}
}

