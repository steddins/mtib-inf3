/** EqualsDemo.java
* 
* Das Programm verdeutlicht den Unterschied zwischen der referenziellen 
* Identitšt (Vergleich der Zugriffsadresse)  und der inhaltlichen 
* (Vergleich der Datenfelder der Instanzvariablen) Identitšt.   
*
* @author Steddin
* @version 1.00, 2017-11-20
*/
public class EqualsDemo {

	public static void main(String[] args) {
		Person hans = new Person();
		Person hugo = new Person();
		if (hugo == hans) 
			System.out.println("Hans und Hugo sind identisch");
		else
			System.out.println("Hans und Hugo sind nicht identisch");
		
		if (hans.equals(hugo)) 
			System.out.println("Hans und Hugo haben identische Daten");
	}

}

class Person {
	int geburtsjahr = 2010;
	int geburtsmonat = 10;
	int geburtstag = 31;
	boolean equals(Person anderePerson) {
		if ((this.geburtsjahr == anderePerson.geburtsjahr) &&
				(this.geburtsmonat == anderePerson.geburtsmonat) &&
				(this.geburtstag == anderePerson.geburtstag))
			return true;
		else 
			return false;
	}
}



