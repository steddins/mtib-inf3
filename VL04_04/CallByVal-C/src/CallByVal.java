/** 
* CallByVal.java
* 
* Primitive Typen werden beim Aufruf einer MEthode 
* immer als Kopie �bergeben. D.h. �nderungen an der 
* Kopie innerhalb der aufgerufenen Methode haben keine
* Auswirkung auf die aufrufende Methode. Sollen die 
* �nderungen an den Aufrufer zur�ckgegeben werden, so
* muss dies immer �ber einen Return value erfolgen.
* 
* @author Steddin
* @version 1.00, 2017-11-20
*/
public class CallByVal {

	static public int power_callByValue(int val) {
		val = val * val;
		return val;
	}
	
	public static void main(String[] args) {

		int z = 2;
		int result;
		
		result = power_callByValue(z);
		System.out.println(z + "^2 = " + result);
	}
}



