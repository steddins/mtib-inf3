/** InitDemo1.java
* 
* Demonstration der Behandlung von initialisierten und nicht initialisierten Variablen.
* Das Programm beinhaltet Fehler. Wo sind die Fehler und wie kommen diese zustande?
*
* @author Steddin
* @version 1.00, 2016-11-18
*/

public class InitDemo1 {
	static int anzAchsen = 2;
	public static void main(String[] args) {
		System.out.println("Anzahl Lenkr�der: " + Kfz.anzLenkrad);
		Kfz ente;
		System.out.println("Anzahl Hupen: " + ente.anzHupen);		
		ente = new Kfz();
		System.out.println("Anzahl R�der vor Initialisierung: " + ente.GetAnzahlRaeder());
		ente.SetAnzahlRaeder(4);
		System.out.println("Anzahl R�der nach Initialisierung: " + ente.GetAnzahlRaeder());
		System.out.println("Anzahl Sitze: " + ente.GetAnzahlSitze());
	}
}

class Kfz {
	static int anzLenkrad;	  //wenn nicht als public deklariert, dann nur im package sichtbar
	private int anzahlRaeder; //Instanzvariable
	public int anzHupen;
	
	void SetAnzahlRaeder(int anz) {
		static final int anzRaederProAchse = 2;
		anzahlRaeder = anzRaederProAchse * anzAchsen;		
	}
	
	int GetAnzahlRaeder() {
		return anzahlRaeder;
	}
	
	int GetAnzahlSitze() {
		public int anzSitze;		// lokale Variable
		return anzSitze;
	}
}


