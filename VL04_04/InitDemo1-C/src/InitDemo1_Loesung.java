/** 
* InitDemo1_Loesung.java
* 
* Demonstration der Behandlung von initialisierten und nicht initialisierten Variablen.
* Das Programm beinhaltet Fehler. Wo sind die Fehler und wie kommen diese zustande?
*
* @author Steddin
* @version 1.00, 2016-11-18
*/

public class InitDemo1_Loesung {
	final static int anzAchsen=2;   // ist als  Klassenvariable im ganzen Package sichtbar
	public static void main(String[] args) {
		System.out.println("Anzahl Lenkr�der: " + Kfz.anzLenkrad);
		Kfz ente;		
		ente = new Kfz();
		//auf eine Instanzvariable kann erst nach der Initialisierung zugeriffen werden:
		System.out.println("Anzahl Hupen: " + ente.anzHupen);	    
		System.out.println("Anzahl R�der vor Initialisierung: " + ente.GetAnzahlRaeder());
		ente.SetAnzahlRaeder(4);
		System.out.println("Anzahl R�der nach Initialisierung: " + ente.GetAnzahlRaeder());
		System.out.println("Anzahl Sitze: " + ente.GetAnzahlSitze());
	}
}

class Kfz {
	static int anzLenkrad;	  //wenn nicht als public deklariert, dann nur im package sichtbar
	private int anzahlRaeder; //Instanzvariable
	public int anzHupen = 2;
	
	void SetAnzahlRaeder(int anz) {
		final int anzRaederProAchse = 2;
		anzahlRaeder = anzRaederProAchse * InitDemo1_Loesung.anzAchsen;
		//Klassenvariable muss �ber Klassenname referenziert werden
		//vorher: anzahlRaeder = anzRaederProAchse * anzAchsen;		
	}
	
	int GetAnzahlRaeder() {
		return anzahlRaeder;
	}
	
	int GetAnzahlSitze() {
		// lokale Variable kann nicht als public deklariert werden
		// lokale Variable muss vor dem ersten Zugriff initialisiert werden.
		int anzSitze;
		//vorher: public int anzSitze;		
		// Ohne Initialisierung wird das Programm nicht �bersetzt!	
		anzSitze = 4;   
		return anzSitze;
	}
}

