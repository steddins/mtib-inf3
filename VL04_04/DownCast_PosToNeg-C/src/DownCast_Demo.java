/**
 * DownCast_Demo.java
 * 
 * Programm zeigt, dass der downcast einer Zahl zu einem unerwarteten 
 * Ergebnis f�hren kann. L�sung des Problems: Verwendung von ...
 * 
 * @author stedS
 * @version 1.0.0
 */
public class DownCast_Demo {

	public static void main(String[] args) {
		int integerZahl = 0x000001FF;	//entspricht +511
		byte byteZahl = (byte)integerZahl;
		System.out.println("integerZahl = "+integerZahl);
		System.out.println("byteZahl = "+byteZahl);
	}
}




