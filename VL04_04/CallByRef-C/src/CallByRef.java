/** 
* CallByRef.java
* 
* Das Programm verdeutlicht, dass die �bergabe eines Referenzdatentyps
* als Parameter nicht automatisch bedeutet, dass die �nderungen am 
* �bergebenen Parameter nicht unbedingt bedeuten, dass diese �nderungen
* auch in der aufrufenden Methode sichtbar sind. Der Grund hierf�r liegt
* darin, dass beim unboxing und autoboxing ggf. neue Instanzen des urspr�nglichen
* Referenzdatentyps erzeugt werden. Die aufrufende Methode wird hier�ber nicht 
* informiert. Es ist daher immer ratsam, eine Referenz auf das ge�nderte Objekt als 
* return-Wert zur�ckzugeben.
* 
* @author Steddin
* @version 1.00, 2017-11-20
*/

public class CallByRef {

	static public Integer power_callByRef(Integer val) {
		val = val * val;
		return val;
	}
	
	public static void main(String[] args) {

		Integer z, zReturn;
		z = 2;	//Wrapper Klasse wird �ber Autoboxing erzeugt
		
		zReturn = power_callByRef(z);
		System.out.println("Referenz: "+ z + "^2 = " + z);
		System.out.println("Rueckgabe: "+ z + "^2 = " + zReturn);
	}
}



