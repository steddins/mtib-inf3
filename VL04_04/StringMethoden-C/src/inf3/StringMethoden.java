/** StringMethoden.java
* 
* Das Programm demonstriert den Einsatz einiger Methoden,
* die von der Klasse String zur Bearbeitung von Strings 
* bereitgestellt wird.
*
* @author Steddin
* @version 1.00, 2016-11-22
*/
package inf3;

public class StringMethoden {
	public static void main(String[] args) {
		char euroZeichen = '\u20ac';
		String preis = String.valueOf(10.0);
		int cent = 0;
		System.out.println(preis+cent+euroZeichen);
		String bananenPreis = "Wucher-Bananen (kg) :\t"+preis+cent+euroZeichen+"\n\r";
		System.out.print(bananenPreis.substring(bananenPreis.indexOf("-")+1));
	}
}


