/** 
* LokaleVars.java
* 
* Das Programm demonstriert die Möglichkeit Instanzvariablen und 
* Variablen innerhalb von Methoden mit gleichem Namen zu deklarieren
*
* @author Steddin
* @version 1.00, 2017-11-19
*/
package inf3;

class MyClass {
	int value = 100;
	void printValue() {
		int value = 200;
		System.out.println("Method printValue from MyClass: "+value);
		if (value > 100) {
			int value;
			value = 10;
			System.out.println("Method printValue from MyClass, inner block: "+value);
			System.out.println("Method printValue from MyClass, "+
							   "access to instance variable: "+ this.value);
		}	
	}
}

public class LokaleVars {
	public static void main(String[] args) {
		MyClass myClass = new MyClass();
		myClass.printValue();
	}
}
