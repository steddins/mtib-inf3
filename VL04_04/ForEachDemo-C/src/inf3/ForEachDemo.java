/** ForEachDemo.java
* 
* Das Programm zeigt, dass Änderungen eines Array-Elementes innerhalb einer for-each-Schleife
* das Array nicht verändern
*
* @author Steddin
* @version 1.00, 2017-11-28
*/
package inf3;
public class ForEachDemo {

	public static void main(String[] args) {
		Integer myArray[] = {0,1,2,3};
		int ii = 0;
		for (Integer v: myArray) {
			v += 10;
			System.out.println("v["+ ii++ +"] = " + v);
		}
		myArray[2] = myArray[2] + 10;
		
		ii = 0;
		for (Integer v: myArray) {			
			System.out.println("v["+ ii++ +"] = " + v);
		}
	}
}


