/** 
* Doublefehler.java
* 
* Das Programm zeigt, dass bei der Berechnung von Double-Werten
* Ergebnisse auftreten k�nnen, die nicht erwartet werden. Ursache hierf�r
* sind die bei der Berechnung von Double-Werten auftretenden Rundungsfehler.
*
* @author Steddin
* @version 1.00, 2016-11-22
*/

public class Doublefehler {

	public static void main(String[] args) {

		double a1 = 3.01;
		double a2 = 2.01;
		double a3 = 1.99;
		double b = 0.01;
		
		System.out.println("3.01 - 0.01 = " + (a1 - b));
		System.out.println("2.01 - 0.01 = " + (a2 - b));
		System.out.println("2.01 - 0.01 - 2.0 = " + (a2 - b - 2.0));
		System.out.println("Vergleich (1.99 + 0.01) == (2.01 - 0.01) ergibt: " + ((a3+b) == (a2-b)));
	}
}

