/** TreeSetDemo.java
* 
* Das Programm demonstriert die Eigenschaften eines TreeSets:
* - Ein Element kann nicht doppelt enthalten sein
* - Die Elemente werden ihrer Ordnung entsprechend einsortiert
* Quelle: Dirk Louis, Peter M�ller: Java-Tutorium (Hanser 2016)
* @author -
* @version 1.00, 2017-11-28
*/package inf3;

import java.util.Random;
import java.util.TreeSet;

public class TreeSetDemo {

	public static void main(String[] args) {
		TreeSet<Integer> gezogen = new TreeSet<Integer>();
		Random zufall = new Random();
		while (gezogen.size() != 6) {
			int z = zufall.nextInt(50);
			if (z == 0)
				continue;
			// zahl speichern
			boolean status = gezogen.add(z);
		}
		System.out.println("Die Lottozahlen:");
		for (Integer i : gezogen)
			System.out.print(i + " ");
	}
}


