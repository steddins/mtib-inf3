/** 
* ReferenzielleIdent.java
* 
* Das Programm zeigt, dass bei der bei der Zuweisung von Objekt ws2016
* an ws2017 nicht der Inhalt von ws2016 übertragen sondern stattdessen 
* die Referenz (Adresse) von ws2016 auf ws 2017 übertragen wird.
* 
* Das ursprüngliche Objekt ws2017 verwaist auf diese Weise: Es gibt keine 
* Referenzvariable mehr, die auf dieses Objekt verweist. Demzufolge ist das 
* Objekt im Programm nicht mehr zugänglich. 
*
* @author Steddin
* @version 1.00, 2017-11-20
*/
public class ReferenzielleIdent {

	public static void main(String[] args) {
		MetiJahrgang ws2017 = new MetiJahrgang();
		MetiJahrgang ws2016 = new MetiJahrgang();
		ws2016.anzStud = 49;
		ws2017 = ws2016;
		ws2017.anzStud = 51;
		System.out.println("Anz. Stud. WS2017: "+ws2017.anzStud);
		System.out.println("Anz. Stud. WS2016: "+ws2016.anzStud);
	}
}

class MetiJahrgang {
	public int anzStud =0;
}



