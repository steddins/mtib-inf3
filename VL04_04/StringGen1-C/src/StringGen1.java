/** StringGen1.java
* 
* Programm demonstriert, bei welchen Initialisierungen bzw.
* Zuweisungen mit referenzieller Identität oder mit Werteidentität
* zu rechnen ist. Es werden außerdem noch einzelnen Methoden 
* demonstriert, welche von der Klasse String zur Verfügung gestellt werden.
* 
* Die gesamte Übersicht zu den zur Verfügung stehenden String-Methoden
* findet sich in der Dokumentation von Oracle zu Klasse String.
* 
* @author Steddin
* @version 2.00, 2017-12-05
*/
public class StringGen1 {

	public static void main(String[] args) {
		
		char[] cArr1 = {'M','e','t','i'};
		char cArr2[] = {'M','e','t','i'};
		char[] cArr3 = cArr1;

		String s1 = new String();
		s1 = "Meti";
		String s2 = "Meti";				//Besonderheit: Instanz ohne new  (wie bei Wrapper-Typ)
		String s3 = new String (cArr1);
		String s4 = new String (cArr2);
		
		if(cArr1 == cArr2) 		System.out.println("Vergleich1: cArr1 und cArr2 sind gleich");
		if(cArr1 == cArr3) 		System.out.println("Vergleich2: cArr1 und cArr3 sind gleich");
		if(s3 == s4) 			System.out.println("Vergleich3: cArr1 und cArr2 sind gleich");
		if(s3.equals(s4))		System.out.println("Vergleich4: cArr1 und cArr2 sind inhaltlich gleich");
		
		s4 = s4.replace('M', 'Y');
		System.out.println(s4 + " und " + s3 + " sind nicht das Gleiche");
		String s2Copy = s2;
		s2 += s4;
		if (s2!= s2Copy) {
			System.out.println("s2 wurde neu angelegt");
		} else {
			System.out.println("s2 ist unverändert");
		}
		System.out.println(s2);
		s4 = s1.concat(s4);
		System.out.println(s4);
	}
}



