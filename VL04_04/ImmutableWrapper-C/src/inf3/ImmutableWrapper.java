/** 
* ImmutableWrapper.java
* 
* Das Programm zeigt, dass bei kleinen Integer-Werten die Speicherung 
* �hnlich wie beim Java String-Pool erfolgt: f�r wertgleiche Objekte
* werden nicht getrennte Objekte angelegt, stattdessen wird hierf�r
* ein gemeinsames Objekt angelegt. <p>
* Hieran l�sst sich erkennen, warum Integer und String immutable sind:
* wird dem Objekt ein neuer Wert zugewiesen, so w�rden alle andere Referenzen
* auf dieses Objekt ebenfalls von der Wert�nderung betroffen sein. Aus diesem 
* Grunde muss also bei jeder �nderung ein neues Objekt angelegt werden. <p>
* 
* Java implementiert dieses Verhalten, um die Performance zu optimieren. <p>
* Siehe auch: http://www.itcsolutions.eu/2011/02/12/tutorial-java-scjp-12-immutable-string-and-integer/ 
* (Zugriff 2016-11-21)
*
* @author Steddin
* @version 1.00, 2016-11-21
*/package inf3;

public class ImmutableWrapper {

	public static void main(String[] args) {
        Integer i1 =100;            //use constant values
        Integer i2 =100;            //use small values
 
        System.out.println("Integer i1 = i2 = 100 (-128 < i < 127");
        
        if(i1 == i2)        //compare the references
            System.out.println("i1 und i2 zeigen auf ein identisches Objekt");
        else
            System.out.println("i1 und i2 zeigen auf unterschiedliche Objekte");
 
        i1 = 300;          //use bigger values
        i2 = 300;

        System.out.println("Integer i1 = i2 = 300 (-128 >= i || ii > 127");        
 
        if(i1 == i2)        //compare the references
            System.out.println("i1 und i2 zeigen auf ein identisches Objekt");
        else
            System.out.println("i1 und i2 zeigen auf unterschiedliche Objekte");
	}

}
