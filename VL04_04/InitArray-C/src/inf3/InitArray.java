package inf3;
public class InitArray {
	public static void main(String[] args) {
		MyArrayClass myAC = new MyArrayClass();
		myAC.initArrays();
		System.out.print("\nInstanzvariable instance_intArr: ");
		for (int ii: myAC.instance_intArr) System.out.print(ii + "; ");
		System.out.print("\nInstanzvariable instance_IntegerArr: ");
		for (Integer ii: myAC.instance_IntegerArr) System.out.print(ii + "; ");
	}
}
class MyArrayClass {
	int instance_intArr[];
	Integer instance_IntegerArr[];
	
	void initArrays() {
		int local_intArray[] = new int[3];
		Integer local_IntegerArray[] = new Integer[3];
		instance_intArr = new int[3];
		instance_IntegerArr = new Integer[3];
		System.out.print("\nlocal_intArr: ");
		for (int ii: local_intArray) System.out.print(ii + "; ");
		System.out.print("\nlocal_IntegerArr: ");
		for (Integer ii: local_IntegerArray) System.out.print(ii + "; ");
	}
}
