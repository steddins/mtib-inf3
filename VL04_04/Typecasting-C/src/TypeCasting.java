/** 
* Typecasting.java
* 
* Das Programm verdeutlicht Effekte, die bei der Durchf�hrung des expliziten
* Typecasting auftreten. Die Ermittlung der Ergebnisse setzt die Kenntnis
* der Bildung von negativen Zahlen �ber das Zweierkomplement voraus.
*
* @author Steddin
* @version 1.00, 2016-11-22
*/
public class TypeCasting {

	public static void main(String[] args) {
		
		int int32_A, int32_B, int32_Res;
		short int16_A, int16_B, int16_Res;
		
		int32_A = 0x7FFFFFFF;
		int32_A = 2_147_483_647;	
		
		int32_Res = int32_A * 2;	
		int32_Res = int32_A * 4;	
		
		int32_Res = (int32_A * 2) / 2;	
		int32_Res = (int32_A / 2) * 2;	
		
		int32_A = 0x7FFFFFFF;
		int32_B = -1;				
		int16_Res = (short)int32_A;	
		int16_Res = (short)int32_B;	
		
		int32_Res = int32_A >> 16;	
		int32_Res = int32_B >> 16;	
		int32_Res = int32_B / 0x10000; 	
	
		int32_A = 0xFFFF0000;
		int32_Res = int32_A >> 4;
		
		int32_A = 0x7F7F0001;
		int32_B = 0x80000001;
		
		int16_Res = (short)int32_A;
		int16_Res = (short)int32_B;
		
		int32_A = 0x00008001;
		int16_Res = (short)int32_A;	
		
		int32_A = 0xFFFFFFFF;			// == -1
		int32_Res = int32_A >>> 1;		// (logical shift)
		int32_Res = int32_A >> 1;		// (arithmetic shift)	
		int32_Res = int32_A / 2;		// Division
	}
}


