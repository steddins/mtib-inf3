/** 2014-11-03 (Steddin)
* Das Programm dient zur Demonstration der Eigenschaften von Integerzahlen, insbesondere 
* der Vorzeichenarithmetik, sowohl bei Zahlenoperationen (dividieren, shift) als auch beim 
* Typecasting. <p>
* 
* Damit sich das Projekt �bersetzten l�sst, muss die kommentierte Version im Ordner \temp
* vom build-path ausgescholossen werden.
*  
* @author Steddin
* @version 1.00, 2016-11-22
 *
 */
public class TypeCasting {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		int int32_A, int32_B, int32_Res;
		short int16_A, int16_B, int16_Res;
		float float_A, float_B;
		double double_A, double_B;
		char char_A, char_B;
		byte byte_A, byte_B;
		
		int32_A = 0x7FFFFFFF;
		int32_A = 2_147_483_647;	//Zahlenliterale k�nnen bei Java mit Leerzeichen 
									//erweitert werden, um die Lesbarkeit zu erh�hen
		
		int32_Res = int32_A * 2;	// = -2 :  = 0xFFFF FFFE  /auf Zahlenkreis demonstrieren!
		int32_Res = int32_A * 4;	// = -4 :  = 0xFFFF FFFC  /im LSB wird 0 nachgezogen
		//--> keine Warnung vor �berlauf! Aus der positiven Zahl wird eine negative Zahl!
		
		int32_Res = (int32_A * 2) / 2;	// = -1  : Durch Multipl. entsteht �berlauf (neg. Zahl)
										//         Bei Division bleibt Vorzeichen erhalten
		int32_Res = (int32_A / 2) * 2;	// = 2147483646 (0x7FFFFFFE) :
										//         nicht mal das haut hin! ... wegen Rundung bei Division
		
		//--> obwohl die Operationen scheinbar neutral: Ergebnis ist �berraschung (... nicht f�r den Bin�rexperte)
		// Besondere Bedeutung: Klammerung bei Ausdr�cken --> 
		// Praxistipp: viele Klammerebenen (sinnvoll!) verwenden bzw. nur wenige Operationen pro Zeile (auf diese 
		// Weise k�nnen die Zwischenergebnisse mit dem debugger �berpr�ft werden.
		// Assoziativgesetz so anwenden, dass m�glichst keine Unterl�ufe oder �berl�ufe auftreten!
		// Beispiel:
		
		int32_A = 1;
		int32_B = 4;
		//Auswertung von Links nach rechts:
		int32_Res = int32_A * 12 + int32_B / 12 * 3;		//schlecht: Unterlauf 4/12 --> 0: res somit = 12 statt 13
		int32_Res = int32_A * 12 + int32_B * 3 / 12;		//besser: Reihenfolge der Operationen so gew�hlt, dass kein Unterlauf auftritt
															//Ergebnis erwartungsgem�� = 12	
		int32_Res = (int32_A * 12) + ((int32_B * 3) / 12);	//�bertrieben: jedoch direkt erkennbar, welche Zwischenergebnisse auftreten werden
		
		//Zerlegung in Zwischenschritte: 
		//- Vorteil: 	mit Debugger k�nnen Zwischenergebnisse gepr�ft werden
		//- Nachteil: 	Rechenvorschrift nicht mehr direkt erkennbar
		int32_Res = int32_B * 3;
		int32_Res /= 12;
		int int32_Res_Temp = int32_A * 12;
		int32_Res += int32_Res_Temp;
 
		
		
		int32_A = 0x7FFFFFFF;
		int32_B = 0xFFFFFFFF;				    // = - 1
		int16_Res = (short)int32_A;	// = -1 --> Vorzeichen (+) wird nicht erhalten: 
									//			Bei positiver Zahl wird nur das LSByte �bernommen --> negativ!
									//			Soll das Vorzeichen erhalten bleiben --> vorher Shift Operation durchf�hren,
									//          dann wird allerdings das MSB �bernommen und nicht das LSB!
									//			--> auch expliziter Typecast ist gef�hrlich!
		int16_Res = (short)int32_B;	// = -1 --> Vorzeichen Bit bleibt beim Abschneiden des MSByte erhalten
		
	
		//Was passiert bei shift-Operationen mit dem Vorzeichenbit?
		int32_Res = int32_A >> 16;	// = 0x0000 7FFF = 2^15 --> es werden von links 0 nachgezogen: entspricht Division durch 2^16
		int32_Res = int32_B >> 16;	// = 0xFFFF FFFF = -1 --> es werden von links 1 nachgezogen: entspricht ceil(-0.000...1) : Abrundung
		int32_Res = int32_B / 0x00010000; 	// entspr.  int32_B / 2^16  = 0; Division kommt im Gegensatz zum Shift zu betragsm��ig 
											// gerundetem Ergebnis: entspricht Aufrundung (hin zum gr��eren Wert).
 	
		//Merke: Solange bei der SHR-Operation kein Unterlauf auftritt, funktioniert die "Ersatzdivision" auch bei 
		//       negativen Zahlen, da vom MSB her 1er nachgezogen werden --> Zahl im Zweierkomplement korrekt 
		//		 Sobald der Unterlauf auftritt bleibt eine -1 stehen, da es eine -0 in Der Repr�sentation des 
		//		 Zweierkomplements nicht gibt. 
		
		//Division negativer Zahlen mit SHR funktioniert!
		int32_A = 0xFFFF0000;		// = -(2^16) = -65536
		int32_Res = int32_A >> 4; 	// = 0xFFFF F000 = -4096	
		
		
		//Was wird nun eigentlich abgeschnitten: die MSBytes?
		int32_A = 0x7F7F0001;
		int32_B = 0x80000001;
		
		int16_Res = (short)int32_A;		// = 1	--> es wird also das MSB abgeschnitten
		int16_Res = (short)int32_B;		// = 1	// Vorsicht bei den impliziten Typecasts : Vorzeichen bleibt u.U. nicht erhalten
		
		
		int32_A = 0x00008001;
		int16_Res = (short)int32_A;		// = -32767 (0x8001) --> Zahl wird durch Abschneiden der MSB auf einmal negativ!
		
		int32_A = 0xFFFFFFFF;			// == -1
		int32_Res = int32_A >>> 1;		// == 0x7FFFFFFF = +2^15 (logical shift)
		int32_Res = int32_A >> 1;		// == 0xFFFFFFFF --> unerwarteterweise wieder -1 (arithmetic shift)	
		int32_Res = int32_A / 2;		// == 0
	}
}
