/** 
* CallByRefArray.java
* 
* Das Programm zeigt, dass bei der Übergabe eines Arrays an eine 
* Methode bei Änderung des Inhaltes des Arrays kein neues Array Objekt erzeugt
* wird und somit die in der aufgerufenen Methode durchgeführten Änderungen am
* Array anschließend auch im aufrufenden Programmteil sichtbar sind (Verhalten 
* wie bei call by reference).
* 
* Hierbei spielt es keine Rolle, ob im Array Wrappertypen (z.B. Integer) oder Wertetypen
* (z.B. int) enthalten sind. 
*
* @author Steddin
* @version 1.00, 2016-11-23
*/
package inf3;

public class CallByRefArray {

	static void Call_intArray(int ia[]) {
		for (int ii=0; ii< ia.length; ii++) {
			ia[ii] = 9;
		}
	}
	static void Call_IntArray(Integer ia[]) {
		for (int ii=0; ii< ia.length; ii++) {
			ia[ii] = 9;
		}
	}
	
	public static void main(String[] args) {
		int arr_int[] = {1,2,3,4,5};
		Integer arr_Int[] = {1,2,3,4,5};
		
		Call_intArray(arr_int);
		System.out.print("int-Array nach Methodenaufruf: ");
		for (int ii=0; ii< arr_int.length; ii++) {
			System.out.print(arr_int[ii] + ", ");
		}
		System.out.println();
		
		Call_IntArray(arr_Int);
		System.out.print("Integer-Array nach Methodenaufruf: ");
		for (int ii=0; ii< arr_Int.length; ii++) {
			System.out.print(arr_int[ii] + ", ");
		}
		System.out.println();
	}
}


