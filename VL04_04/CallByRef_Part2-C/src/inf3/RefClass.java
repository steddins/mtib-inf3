package inf3;

public class RefClass {
	private int myPrivateValue = 0;
	public int myPublicValue = 0;
	
	void ChangeMyPrivateValue() {
		myPrivateValue++;
	}
	
	int getPrivVal() {
		return myPrivateValue;
	}

}
