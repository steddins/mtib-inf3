/** VL04-04_CallByRef_Part2.java
* 
* Das Programm zeigt, dass Wrapper-Typen, die als Parameter an eine Funktion �bergeben werden,
* anders behandelt werden als Objekte eines anderen Referenzdatentyps: <p>
* W�hrend eine �nderung des Wertes einer als Parameter �bergebenen Wrapper-Klasse zur Generierung 
* eines neuen Wrapper-Objektes f�hrt und somit die �nderung nicht an dem �bergebenen Wrapper-Parameter
* durchgef�hrt wird (also kein call by reference), findet bei der �bergabe eines nicht-Wrapper-Objekts
* tats�chlich ein call by reference statt. <p>
* 
*  Somit gilt: Bei Wrapper-Typen, die als Parameter an eine Methode �bergeben werden gibt es kein call by
*  reference sondern nur ein call by value. Eine in der Methode mit dem �bergebenen Wrapper-Objekt durchgef�hrte 
*  �nderung wird immer an einer Kopie des Parameters durchgef�hrt. Die �nderung muss daher in Form eines 
*  Return-Wertes zur�ckgereicht werden. <p>
*  
*  Der Grund hierf�r ist, dass alle Wrapper-Typen "immutable" sind: <p>
*  Wird diesen Typen ein Wert zugewiesen, so l�sst sich dieser nicht mehr �ndern; stattdessen muss bei einer 
*  �nderung ein neues Objekt angelegt werden. Dieses Verhalten findet sich auch beim Typ String. 
* 
*
* @author Steddin
* @version 1.00, 2016-11-21
*/
package inf3;

public class CallByRef_Part2 {

	static RefClass ChangeValue(RefClass rC) {
		rC.myPublicValue++;
		rC.ChangeMyPrivateValue();
		return rC;
	}
	
	static Integer ChangeIntValue(Integer rI) {
		rI = rI + 1;
		//rI++;  //gleiches Ergebnis, wenn Inkrement aufgerufen wird
		return rI;
	}
	
	
	public static void main(String[] args) {
		RefClass rC = new RefClass();
		RefClass new_rC;
		new_rC = ChangeValue(rC);
		System.out.println("Per return zur�ckgegebene Klasse: " + new_rC.myPublicValue + "  "+new_rC.getPrivVal());
		System.out.println("Per Referenz �bergebene Klasse: " + rC.getPrivVal() + "  " + rC.getPrivVal());		
		if (new_rC == rC) 
			System.out.println("Die als Return Wert zur�ckgegenene Klasse ist mit der als Parameter �bergebenen Klasse identisch");
		else
			System.out.println("Die als Return Wert zur�ckgegenene Klasse ist mit der als Parameter �bergebenen Klasse nicht identisch");
		
		Integer refWrapperClass = 0;
		Integer new_refWrapperClass;
		
		new_refWrapperClass = ChangeIntValue(refWrapperClass);
		System.out.println("Per return zur�ckgegebene Klasse: " + new_refWrapperClass);
		System.out.println("Per Referenz �bergebene Klasse: " + refWrapperClass);		
		if (new_refWrapperClass == refWrapperClass) 
			System.out.println("Die als Return Wert zur�ckgegenene Klasse ist mit der als Parameter �bergebenen Klasse identisch");
		else
			System.out.println("Die als Return Wert zur�ckgegenene Klasse ist mit der als Parameter �bergebenen Klasse nicht identisch");
		
	}

}
