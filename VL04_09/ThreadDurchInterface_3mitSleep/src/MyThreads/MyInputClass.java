package MyThreads;
import java.util.Scanner;

public class MyInputClass extends Thread {

	public void run() {
		Scanner myScanner = new Scanner(System.in);
		int ii = 0;
		while (true) {
			try {
				sleep(200);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			ii = myScanner.nextInt();
			System.out.println("\nEingabe: " + ii);	
		}
	}
}


