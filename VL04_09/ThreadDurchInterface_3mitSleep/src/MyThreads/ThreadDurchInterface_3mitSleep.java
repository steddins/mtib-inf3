package MyThreads;
public class ThreadDurchInterface_3mitSleep {

	public static void main(String[] args) {

		//zuerst die Objekte anlegen, die von Thread nicht erben k�nnen
		//aber daf�r das Interface Runnable unterst�tzen: 
		MyOutputClassChild myRunnable1 = new MyOutputClassChild("Hallo",4);
		MyOutputClassChild myRunnable2 = new MyOutputClassChild("Meti",0);		
		
		//nun alle gew�nschten Threads anlegen:
		MyInputClass myThread1 	= new MyInputClass();
		Thread myThread2 		= new Thread(myRunnable1);
		Thread myThread3 		= new Thread(myRunnable2);
				
		//Thread-Objekte starten
		myThread1.start();
		myThread2.start();
		myThread3.start();
	}
}


