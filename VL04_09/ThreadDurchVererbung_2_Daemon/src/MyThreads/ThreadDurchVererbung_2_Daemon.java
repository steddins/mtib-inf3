/** Programm zur Demonstration der Abh�ngigkeit von Hintergrund Threads
 * und Vordergrund-Threads. 
 * 
 * @version 1.0 (2017-01-12) 
 */
package MyThreads;
public class ThreadDurchVererbung_2_Daemon {
	/** Durch Auskommentieren des Befehls zum Setzen von Daemon Threads
	 * kann das Verhalten des Programms untersucht werden.
	 * @param args Runntimeparameter nicht genutzt
	 * @author stedS
	 */
	public static void main(String[] args) {
		MyInputClass myThread1 = new MyInputClass();
		MyOutputClass myThread2 = new MyOutputClass("Hallo");
		MyOutputClass myThread3 = new MyOutputClass("Meti");
		//myThread2.setDaemon(true);
		//myThread3.setDaemon(true);
		myThread1.start();
		myThread2.start();		
		myThread3.start();
	}
}


//System.exit(3); //kann verwendet werden, um Programm endg�ltig abzuschie�en