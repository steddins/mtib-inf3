package MyThreads;

public class MyOutputClass extends Thread {
	private String text;
	
	MyOutputClass(String text)
	{
		this.text = text;
	}
	
	public void run() {
		long kk = 0;
		while (true) {
			if (kk++ % 10000000 == 0) {
				System.out.println(text + " ");
			}
		}		
	}
}

