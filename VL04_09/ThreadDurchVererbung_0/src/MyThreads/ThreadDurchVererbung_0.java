package MyThreads;
/** Das Programm zeigt, wie das Betriebssystem (die Java VM) verschiedenen Threads 
* Rechenzeit zuordnet. 
* 
* <p>Der Main-Thread des Programms gibt hierzu �ber eine Endlosschleife 
* Sternchen auf der Konsole aus. Parallel hierzu wird ein zweiter Thread gestartet, der 
* auf eine Eingabe von der Tastatur wartet und ein eigegebene Zeichen wieder auf der Konsole
* ausgibt, sobald die Returntaste gedr�ckt wird.
* 
* <p>Es ist zu erkennen, dass die Ausgabe des Echos der Tastatureingabe offenbar die Ausgabe der
* Sternchen kurzzeitig unterbricht.
*
* @version 2.00, 2020-01-06
*/
public class ThreadDurchVererbung_0 {
	/** Thread startet weiteren Thread zum Einlesen von Zahlen �ber die Tastatur und 
	 * gibt anschlie�end Sternchen auf dem Bildschirm aus.
	 * @param args Keine Auswertung von Runtimeparametern
	 * @author Steddin
	 */
	public static void main(String[] args) {
		MyThreadClass myThread = new MyThreadClass();
		myThread.start();
		int kk = 0;
		while (true) {
			if ((kk%10_000_000) == 0) {
				System.out.print("*");
			}
			kk++;
			if (kk%100_000_000==0) {
				System.out.print("\n");
			}
		}
	}
}



