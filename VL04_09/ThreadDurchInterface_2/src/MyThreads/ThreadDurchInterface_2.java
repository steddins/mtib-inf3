/** Programm zur Demonstration des Einsatzes von Runnable: Die Klasse,
 * welche an den Thread �bergeben wird erbt die Methode run() und damit die
 * Implementierung der Schnittstelle Runnable von seiner Elternklasse. 
 * 
 * @author stedS
 * @version 1.0 (2017-01-12) 
 */
package MyThreads;
public class ThreadDurchInterface_2 {
	/** Hauptprogramm legt 3 Threads an, von denen 2 Threads die run-Methode 
	 * einer Klasse verwenden, welche das Interface Runnable implementiert.
	 * @param args Es werden keine Runtimeparameter verwendet
	 * @author stedS
	 */
	public static void main(String[] args) {

		//zuerst die Objekte anlegen, die von Thread nicht erben k�nnen
		//aber daf�r das Interface Runnable unterst�tzen: 
		MyOutputClassChild myRunnable1 	= new MyOutputClassChild("Hallo",4);
		MyOutputClass myRunnable2 		= new MyOutputClass("Meti");		
		
		//nun alle gew�nschten Threads anlegen:
		MyInputClass myThread1 	= new MyInputClass();
		Thread myThread2 		= new Thread(myRunnable1);
		Thread myThread3 		= new Thread(myRunnable2);
		
		//Thread-Objekte starten
		myThread1.start();
		myThread2.start();
		myThread3.start();
	}
}


