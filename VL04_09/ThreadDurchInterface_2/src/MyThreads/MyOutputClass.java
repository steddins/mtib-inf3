package MyThreads;
/**
 * Subklassen dieser Klasse erben die Implementierung von Runnable.
 * @author stedS
 *
 */
public class MyOutputClass implements Runnable {
	private String text;
	int cnt = 0;
	
	MyOutputClass(String text)
	{
		this.text = text;
	}
	
	protected void MachWas() 
	{ /*kann in der Kindklasse modifiziert werden*/ };
	
	public void run() {
		long kk = 0;
		while (true) {
			if (kk++ % 10_000_000 == 0) {
				cnt++;
				System.out.print(text + " ");
				MachWas();
}}}}


