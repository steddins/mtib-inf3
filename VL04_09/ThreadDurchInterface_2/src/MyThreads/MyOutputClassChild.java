package MyThreads;
/**
 * Die Klasse zeigt, dass man die Inplementierung der Schnittstelle Runnable 
 * von seiner Elternklasse erben kann.
 * @author stedS
 */
public class MyOutputClassChild extends MyOutputClass {
	
	private int crlfCnt = 0;
	
	public MyOutputClassChild(String text, int cnt) {
		super(text);
		crlfCnt = cnt;
	}

	@Override
	protected void MachWas() {
		if (crlfCnt != 0) {
			if (cnt % crlfCnt == 0) {
				System.out.println("CRLF");
}}}}


