/** MultitaskReqExample.java
* 
* Das Programm stellt einen Versuch dar, ein Konsolenprogramm 2 Aufgaben erledigen
* zu lassen:
* a) Fortlaufende Ausgabe der Zeit
* b) Einlesen eines Zeichens, um das Progammende einzuleiten
* Es wird auf das Zeichen 0 getestet.
* Leider erledigt das Programm die Aufgabe nicht wie erwartet:
* Der Read-Befehl blockiert die Schleife, solange kein Zeichen eingegeben wird.
* Wie l�sst sich das Problem beheben?
*
* @author Steddin
* @version 1.00, 2017-01-12
*/
package inf3;
import java.io.IOException;
import java.io.InputStreamReader;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
//import static java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
public class MultiTaskReqExample {

	public static void main(String[] args) {
		
		InputStreamReader isr = new InputStreamReader(System.in);
		while (true) {
			LocalDateTime timePoint = LocalDateTime.now();
			String timeString = timePoint.truncatedTo(ChronoUnit.SECONDS).format(DateTimeFormatter.ISO_LOCAL_TIME);
			System.out.print(timeString);
			char[] cbuf = new char[3];
			try {
				isr.read(cbuf);
				if (cbuf[0] == '0') {
					break;
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		System.out.println("Programmende");		
	}
}


