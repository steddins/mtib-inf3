package inf3;
public class Thread_Koch1 extends Thread {
	Object teigschuessel;
	Object ruehrgeraet;
	Thread_Koch1 (Object teigschuessel, Object ruehrgeraet) {
		this.teigschuessel = teigschuessel;
		this.ruehrgeraet = ruehrgeraet;
	}
	
	public void run() {
		synchronized (teigschuessel) {
			System.out.println(this.getName() + " hat Teigsch�ssel und wartet auf R�hrger�t");
			try {
				Thread.sleep(200);
			} catch (InterruptedException e) { 	}
			synchronized (ruehrgeraet) {
				System.out.println(this.getName() + " r�hrt Teig");	
				try {
					Thread.sleep(200);
				} catch (InterruptedException e) { 	}
			}
		}
	}
}

