package inf3;
public class Thread_Koch2 extends Thread {
	Object teigschuessel;
	Object ruehrgeraet;
	Thread_Koch2 (Object teigschuessel, Object ruehrgeraet) {
		this.teigschuessel = teigschuessel;
		this.ruehrgeraet = ruehrgeraet;
	}
	
	public void run() {
		synchronized (ruehrgeraet) {
			System.out.println(this.getName() + " hat R�hrger�t und wartet auf Teigsch�ssel");
			try {
				Thread.sleep(200);
			} catch (InterruptedException e) { 	}
			synchronized (teigschuessel) {
				System.out.println(this.getName() + " r�hrt Teig");	
				try {
					Thread.sleep(200);
				} catch (InterruptedException e) { 	}
			}
		}
	}
}

