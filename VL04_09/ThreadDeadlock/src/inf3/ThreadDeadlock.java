/** Programm zur Demonstration ein Deadlocks
 * 
 * Greifen unterschiedliche Threads auf mehrere gemeinsam genutzte Resourcen zu, dann
 * kann dies zu Verklemmungen f�hren. Durch vorgegebene Reihenfolge beim Zugriff l�sst sich
 * das Problem beheben.
 * 
 * @author stedS
 * @version 2.0 (2021-01-25) 
 */
package inf3;
public class ThreadDeadlock {
	public static void main(String[] args) {
		Object teigschuessel = new Object();
		Object ruehrgeraet = new Object();
		Thread_Koch1 kochA = new Thread_Koch1(teigschuessel, ruehrgeraet);
		Thread_Koch2 kochB = new Thread_Koch2(teigschuessel, ruehrgeraet);
		//Thread_Koch1 kochB = new Thread_Koch1(teigschuessel, ruehrgeraet);
		kochA.start();
		kochB.start();
		try {
			kochA.join();
			kochB.join();
		} catch (InterruptedException e) {
			System.out.println("join interrupted");
		}
		System.out.println("Kuchen fertig");
	}
}


