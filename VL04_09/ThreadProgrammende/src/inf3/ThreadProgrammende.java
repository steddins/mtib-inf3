package inf3;
/** Programm zur Demonstration des Verhaltens von Vordergrund- und 
* Hintergrund-Threads bei Programmende 
* 
* <ul>
* 	<li> Es soll gezeigt werden, in welchem Moment der Programmausführung
* 		 der Daemon-Thread automatisch gestoppt wird.</li>
*   <li> Es zeigt sich, dass das Programm erst dann zu Ende ist, wenn alle
*        User Threads beendet sind. Erst dann wird der Daemon zerstört. Dies
*        geschieht hier nicht bereits mit dem Ende von main().</li> 
*   <li> Obwohl der Daemon Thread unabhängig von der vorgegebenen Anzahl der 
*        Thread Durchläufe ist, wird er spätestens abgebrochen, wenn der user
*        Thread beendet wird (qed).</li> 
* </ul>
*
* @author Steddin
* @version 2.00, 2020-01-26 (Steddin) Programm so umgebaut, dass gezeigt wird, 
* wie sich das Programm ohne den Aufruf von sleep(500) verhält. 
* @version 1.01, 2020-01-06
* @version 1.00, 2019-01-18
*/
public class ThreadProgrammende {
	/** Hauptprogramm startet einen Daemon-Thread und 2 User-Thread
	 * @param args Runtimeparameter werden nicht genützt
	 * @author Sven Steddin
	 */

	public static boolean MIT_SLEEP_AUFRUF = true; 
	public static void main(String[] args) {
		System.out.println("main-Thread gestartet ...");
		MyThread mythread1 = new MyThread(1, 5);
		MyThread mythread2 = new MyThread(2, 10);
		mythread2.setDaemon(true);
		mythread1.start();
		mythread2.start();
		System.out.println("main-Thread beendet (Programmende?)");
	}
}

/** Thread zeigt unterschiedliches Verhalten, je nachdem, ob er als user thread oder 
 * als daemon thread erzeugt wird 
 * @author stedS
 */
class MyThread extends Thread {
	int idx; 					/** Kennnummer des Threads */  
	int cnt;					/** Anzahl der Aufrufe des Threads */
	int maxCnt;					/** max. Anzahl der Durchläufe des Threads */
	
	MyThread (int idx, int maxCnt) {
		super();
		cnt = 0;
		this. idx = idx;
		this.maxCnt = maxCnt; 
	}
	public void run() {
		while(true) {
			String t = this.isDaemon() ? "Daemon" : "Normal";
			System.out.println("Thread "+idx+" ["+t+"] : "+ ++cnt +". Durchlauf");
			if (!this.isDaemon() && (cnt >= maxCnt))  {
				System.out.println("Thread "+idx+" ["+t+"] : Abbruch");
				break;
			}
			if (ThreadProgrammende.MIT_SLEEP_AUFRUF) {
				try {
					sleep(500);	// was passiert, wenn sleep() nicht aufgerufen wird?
				} catch (InterruptedException e) { e.printStackTrace(); }
			}
		}
	}
}
