/** Programm zur Demonstration der Threadsynchronisation
 * durch den Einsatz eines Monitors: Zwei Threads sollen 
 * jeweils 10 mal die im Konstruktor �bergebene Ziffer in eine
 * Zeile schreiben. Erst wenn die Methode SchreibeZeile() gegen 
 * Unterbrechungen gesperrt wird, indem sie mit synchronized er-
 * weitert wird, erh�lt man die gew�nschte gleichm��ige Ausgabe 
 * der Zahlenwerte.
 * 
 * Um sicherzustellen, dass die Ausgabe-Threads auf ein gemeinsames
 * Sperrobjekt zugreifen, muss die Methode SchreibeZeile() als static
 * deklariert werden. Der Lock steht dann der Klasse Textausgabe nur 
 * einmal gemeinsam zur Verf�gung und wird nicht f�r jedes instanzierte
 * Objekt separat angelegt (in diesem Fall verfehlt er die gew�nschte
 * Wirkung.
 * 
 * Die Thread-Methode yield() erweist sich als f�r die Aufgabe untauglich.
 * Es ist weder sichergestellt, dass der thread beim Aufruf von yield()
 * tats�chlich den running state verl�sst, noch, dass ein thread unmittelbar
 * nach Verlassen des running Zustandes nicht sofort wieder vom OS
 * aktiviert wird.
 * 
 * Die vorgestellte L�sung stellt nun zwar sicher, dass die Ausgabe einer 
 * Textzeile nicht unterbrochen wird, sie bewirkt jedoch nicht die 
 * wechselweise Ausgabe der Textzeilen der einzelnen Threads.
 * 
 * @author stedS
 * @version 2.0 (2021-01-25) 
 */
package inf3;
public class ThreadsMonitor1 {
	char c;
	public static void main(String[] args) {
		Thread_TextAusgabe th1 = new Thread_TextAusgabe('1');
		Thread_TextAusgabe th2 = new Thread_TextAusgabe('2');
		th1.start();
		th2.start();
	}
}


