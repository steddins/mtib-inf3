package inf3;
import java.util.ArrayList;

public class PriorityThread extends Thread {
	volatile static long				lastThreadID = 99999999;
	volatile static boolean 			gameOver = false;
	volatile long						result = 0L;
	volatile long			 			taskSwitches = 0;
	volatile long 						count = 0;
	
	PriorityThread(int prio) {
		setPriority(prio);
		lastThreadID = Thread.currentThread().getId();
	}
	
	void job() {
		long threadID = Thread.currentThread().getId(); 
		if (lastThreadID != threadID) {
			lastThreadID = threadID; 
			taskSwitches++;
		}
		result += threadID;
		if (++count >= 10_000_000) gameOver = true;
	}
	
	String getResultStr() {
		return ("thread prio ("+ getPriority() + 
				") beendet:\tOperationen: " + count + ";\tswitches: "+ 
				taskSwitches+";\t Op/slot: "+ (double)count/taskSwitches);
	}
	
	public void run() {
		while (!gameOver) { 
			job(); 
			//Thread.yield();  //was bewirkt der Aufruf von yield() an dieser Stelle? 
		}
	}
}

