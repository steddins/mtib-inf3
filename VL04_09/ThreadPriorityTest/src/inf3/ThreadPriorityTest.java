/** ThreadPriorityTest.java
* Das Programm soll zeigen, wie sich die Zuordnung einer Thread Priorit�t 
* auf die H�ufigkeit der Threadaufrufe und die L�nge der jedem Thread 
* zugeordneten Zeitscheiben auswirkt. 
* 
* Der Vergleich zwischen Ausf�hrung des Programms mit und ohne yield()-Befehl
* verdeutlicht die Strategie der JVM bei der Ber�cksichtigung untersch. Priorit�ten. 
*  
* @author Steddin
* @version 2.00, 2020-01-26 (Steddin) Codeumfang reduziert; Ablauf vereinfacht
* @version 1.00, 2017-01-19
*/
package inf3;
public class ThreadPriorityTest {
	static PriorityThread [] priorObjArr = 
			new PriorityThread[Thread.MAX_PRIORITY];
	
	public static void main(String[] args) {
		//Threads mit unterschiedlichen Priorit�ten anlegen:
		for(int kk = Thread.MIN_PRIORITY; kk <= Thread.MAX_PRIORITY; kk++) {
			priorObjArr[kk-1] = new PriorityThread(kk);
		}
		//Threads gleichzeitig starten:
		System.out.println("Programmstart ... bitte warten");	
		for (PriorityThread pth : priorObjArr) {
			pth.start();
		}	
		for (PriorityThread pth : priorObjArr) {
			try {	// ... warten, bis alle Threads beendet sind:
				pth.join(); 
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}	
		//Auch nach Durchlauf der run() Methode ist das Thread-Object noch verf�gbar und 
		//es kann auf seine Daten zugegriffen weerden: Ausgabe der Ergebnisse erst nach 
		//Abschluss aller Threads, um die Ausf�hrung der Threads nicht zu st�ren
		for(int kk = 0; kk < Thread.MAX_PRIORITY; kk++) {
			System.out.println(priorObjArr[kk].getResultStr()); 
		}
	}
}




