/** ThreadPriority.java
 * Programm zur Demonstration der in Java verf�gbaren Priorit�tsstufen
 * 
 * @author stedS
 * @version 1.0 (2017-01-19) 
 */
package inf3;
public class ThreadPriority {

	public static void main(String[] args) {
		System.out.println("Minimale Thread-Priorit�t: " + Thread.MIN_PRIORITY);
		System.out.println("Maximale Thread-Priorit�t: " + Thread.MAX_PRIORITY);
		System.out.println("Standard Thread-Priorit�t: " + Thread.NORM_PRIORITY);
		System.out.println("Priorit�t des main Threads: " + Thread.currentThread().getPriority());
	}
}



