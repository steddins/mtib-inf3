/** Programm zur Demonstration des Einsatzes von Runnable: Die Klasse,
 * welche an den Thread �bergeben wird erbt die Methode run(), und damit die
 * Implementierung der Schnittstelle Runnable, von seiner Elternklasse. 
 * 
 * Au�erdem soll demonstriert werden, wie sich der Einsatz der Methode sleep()
 * auf die CPU-Last auswirkt, im Vergleich zum Projekt ThreadDurchInterface_3, welches
 * Programmverz�gerungen �ber Z�hlerschleifen implementiert (busy waiting).
 * 
 * @author stedS
 * @version 1.0 (2017-01-12) 
 */
package MyThreads;
public class ThreadDurchInterface_3 {

	public static void main(String[] args) {

		//zuerst die Objekte anlegen, die von Thread nicht erben k�nnen
		//aber daf�r das Interface Runnable unterst�tzen: 
		MyOutputClassChild myRunnable1 = new MyOutputClassChild("Hallo",4);
		MyOutputClassChild myRunnable2 = new MyOutputClassChild("Meti",0);		
		
		//nun alle gew�nschten Threads anlegen:
		MyInputClass myThread1 	= new MyInputClass();
		Thread myThread2 		= new Thread(myRunnable1);
		Thread myThread3 		= new Thread(myRunnable2);
				
		//Thread-Objekte starten
		myThread1.start();
		myThread2.start();
		myThread3.start();
	}
}


