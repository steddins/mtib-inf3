package MyThreads;
/** Die Klasse definiert die Methode MachWas() als abstract. Sie muss von einer Kindklasse 
 * überschrieben werden. Die Nachkommen dieser Klasse erben wiederum die Implementierung 
 * der Schnittstelle Runnable.
 * @author stedS
 */
public abstract class MyOutputClass {
	private String text;
	int cnt = 0;
	
	MyOutputClass(String text)
	{
		this.text = text;
	}
	
	protected abstract void MachWas();
	
	public void run() {
		long kk = 0;
		while (true) {
			if (kk++ % 1_000_000 == 0) {
				cnt++;
				System.out.print(text + " ");
				MachWas();
			}
		}		
	}
}


