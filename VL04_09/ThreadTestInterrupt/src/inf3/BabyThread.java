package inf3;

public class BabyThread extends Thread {
	private int sleepDuration;

	BabyThread(int sleepDuration, String babyName) {
		super();
		this.sleepDuration = sleepDuration;
		this.setName(babyName);
	}
	
	public void run() {
		System.out.println(this.getName() + " schlafen gelegt");
		try {
			Thread.sleep(sleepDuration);
			System.out.println("\n" + this.getName() + " ausgeschlafen");
		}
		catch (InterruptedException e) {
			System.out.println("\n" + this.getName() + " aufgeweckt und sauer");
		}
	}
}
