/** ThreadTestInterrupt.java
*
* @author Steddin
* @version 1.00, 2017-01-19
*/
package inf3;
public class ThreadTestInterrupt {

	public static void main(String[] args) {
				
		BabyThread[] baby =  new BabyThread[4];
		baby[0] = new BabyThread(8000,"Hans");
		baby[1] = new BabyThread(5000,"Ludwig");
		baby[2] = new BabyThread(5000,"Klara");
		baby[3] = new BabyThread(2000,"Erna");		

		for (BabyThread aa: baby) {
			aa.start();
		}
		
		WeckerThread wecker = new WeckerThread(baby);
		wecker.setDaemon(true);
		wecker.start();
		
		
		for (BabyThread aa: baby) {
			try {  
				while (aa.isAlive()) {
					System.out.print ("... es schl�ft noch jemand ");					
					aa.join(1000);
				}
			} catch (InterruptedException e) { 
			}				
		}
		System.out.println ("Alle Babys wach :)");
	}
}

