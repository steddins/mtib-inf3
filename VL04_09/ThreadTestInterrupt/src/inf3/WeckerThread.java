package inf3;
import java.io.IOException;
import java.io.InputStreamReader;

   
public class WeckerThread extends Thread {

	BabyThread[] babys;
	InputStreamReader isr;
	
	WeckerThread (BabyThread[] babys) {
		super();
		this.babys = babys;	
		isr = new InputStreamReader(System.in); 
	}
	
	public void run() {
		try {
			while (!isr.ready()) {
				try {
					System.out.print(" TickTack ");				
					Thread.sleep(200);
				} catch (InterruptedException e) {
					System.out.println("Deamon abgebrochen");
				}
			}
			System.out.println("Wecker aktiviert");
			for (BabyThread aa: babys) {
				if (aa.isAlive()) {
					aa.interrupt();
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}






