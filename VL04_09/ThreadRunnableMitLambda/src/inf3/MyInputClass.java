package inf3;
import java.util.Scanner;
/** Der Thread wartet auf eine Eingabe von der Tastatur. Der Thread bricht mit einer Exception ab,
 * wenn Zeichen eingegeben werden, die keine Zahlen sind, so dass die Zahlenkonvertierung fehlschlägt.
 * @author stedS
 */
public class MyInputClass extends Thread {

	public void run() {
		Scanner myScanner = new Scanner(System.in);
		int ii = 0;
		while (true) {
			ii = myScanner.nextInt();
			System.out.println("\nEingabe: " + ii);		
}}}


