package inf3;
/** Der Thread gibt im 500ms Takt Sternchen auf der Konsole aus
 * und erzeugt nach jedem 10. Sternchen einen Zeilenumbruch.
 * @author stedS
 */
public class MyOutputClass implements Runnable {
	public void run() {
		int ll = 0;
		while (true) {
			if (ll++ %10 != 0) {					
				System.out.print("*");
			} else { 
				System.out.println();
			}
			try { Thread.sleep(500);	
			} catch (InterruptedException e) { e.printStackTrace(); }
		}		
	}
}




