package inf3;
/** Programm zur Demonstration der �bergabe eines Lambda Ausdrucks an 
* den Konstruktor eines Threads anstelle eines Objekts, welches das Interface
* Runnable unterst�tzt. Durch die Verwendung eines Lambda-Ausdrucks, der die 
* Methode run() darstellt, kann man sich die Definition einer eigenen
* Klasse, welche das Interface Runnable implementiert, sparen. Der Code
* wird somit k�rzer und besser lesbar.
* 
* <ul>
* 	<li> myThread1 wird durch die �bergabe eines Runnable Objekts erzeugt. Dieses
* 		 Object (myRunnable) muss zuvor angelegt werden und es muss die Klasse 
*        MyOutputClass definiert werden, welche die Methode run() implementiert </li>
*   <li> myThread2 wird erstellt, indem anstelle des Runnable-Objects die run()
*   	 Methode direkt als Lambda-Ausdruck an den Thread-Konstruktor �bergeben wird.</li> 
* </ul>
*
* @author Steddin
* @version 1.00, 2020-01-26 (Steddin) Erstausgabe
*/

public class ThreadRunnableMitLambda {
	public static void main(String[] args) {

		//Objekt anlegen, welches Runnable impl. und an Thread-Object �bergeben wird
		MyOutputClass myRunnable = new MyOutputClass();
		//Thread-Objekt erzeugen und Runnable-Object �bergeben
		Thread myThread1 = new Thread(myRunnable);
		//Thread-Object erzeugen und statt Runnable-Object "Lambda-Ausdruck" �bergeben
		Thread myThread2 = new Thread (
			() -> {	 while (true) { 
							System.out.print("+");
							try { Thread.sleep(500);	
							} catch (InterruptedException e) { e.printStackTrace(); }	
					 }
				  }					 );
		myThread1.start();
		myThread2.start();
	}
}

