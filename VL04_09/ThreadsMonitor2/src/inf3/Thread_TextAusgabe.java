package inf3;
public class Thread_TextAusgabe extends Thread {
	char c;
	Object consoleLock;
	Thread_TextAusgabe (char c, Object consoleLock) {
		this.c = c;
		this.consoleLock = consoleLock;
	}
	
	void SchreibeZeile (char c) {
		for (int ii=0; ii< 20; ii++) {
			System.out.print(c + " ");
			//busy waiting ... keine gute Idee!
//			for (int ll=0; ll< 1000000; ll++) {
//				Double kk = Math.sqrt(ll);
//			}
			try {
				Thread.sleep(20);
			} catch (InterruptedException e) {
				System.out.println("interrupted while sleeping");
			}
		}
		System.out.println();
	}
	
	public void run() {
		synchronized (consoleLock) {
			for (int ii=0; ii<5; ii++) {
				SchreibeZeile(c);
				consoleLock.notifyAll();
				try {
					consoleLock.wait();
				} catch (InterruptedException e) {
					;
				}
			}
			consoleLock.notifyAll();
		}
	}
}


