/** Programm zur Demonstration der Threadsynchronisation
 * durch den Einsatz eines Monitors: Zwei Threads sollen 
 * jeweils 10 mal die im Konstruktor übergebene Ziffer in eine
 * Zeile schreiben.  
 * 
 * Durch Verwendung der Methoden wait() und notify() zusamen mit 
 * einem gemeinsam genutzten Lock-Objekt kann erzwungen werden, dass 
 * sich die Threads bei der Textausgabe abwechseln.
 * 
 * @author stedS
 * @version 1.0 (2021-01-25) 
 */
package inf3;
public class ThreadsMonitor2 {
	char c;
	
	public static void main(String[] args) {
		Object console = new Object();
		Thread_TextAusgabe th1 = new Thread_TextAusgabe('1', console);
		Thread_TextAusgabe th2 = new Thread_TextAusgabe('2', console);
		th1.start();
		th2.start();
		try {
			th1.join();
			th2.join();
		} catch (InterruptedException e) {
			System.out.println("join interrupted");
		}
		System.out.println("Programmende");
	}
}


