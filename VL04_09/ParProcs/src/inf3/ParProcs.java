package inf3;
import java.io.IOException;
/** Programm zur Demonstration der Erzeugung unabh�ngiger Prozesse in Java. 
* 
* <ul>
* 	<li> Nach dem Start des Programmes kann der Taskmanager ge�ffnet werden, um zu
* 		 zeigen, dass tats�chlich zwei Programme laufen, obwohl das Hauptprogramm 
* 		 bereits beendet ist. </li>
*   <li> Bei mehrfachem Aufruf des Programmes werden auch die aktivierten Programme
*  	     mehrfach gestartet, woran sich erkennen l�sst, dass jedes Programm seinen
* 		 eigenen Prozessraum erh�lt. </li> 
* </ul>
*
* @author Steddin
* @version 1.01, 2017-01-12
*/
public class ParProcs {

	public static void main(String[] args) {
		ProcessBuilder pb1 = new ProcessBuilder("notepad.exe");
		ProcessBuilder pb2 = new ProcessBuilder("calc.exe");
		try {
			pb1.start();
			pb2.start();
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println("Hauptprogramm beendet!");
	}
}


