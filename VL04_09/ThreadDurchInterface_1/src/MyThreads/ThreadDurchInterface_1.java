/** Programm zur Demonstration von Runnable. 
 * 
 * @author stedS
 * @version 1.0 (2017-01-12) 
 */
package MyThreads;
public class ThreadDurchInterface_1 {
	/** Hauptprogramm legt 3 Threads an, von denen 2 Threads die run-Methode 
	 * einer Klasse verwenden, welche das Interface Runnable implementiert.
	 * @param args Es werden keine Runtimeparameter verwendet
	 * @author stedS
	 */
	public static void main(String[] args) {
		final int COUNT = 5;

		//Objekte anlegen, die keine Unterklassen von Thread sind 
		//daf�r aber das Interface Runnable unterst�tzen: 
		MyOutputClass myRunnable1 = 	new MyOutputClass("Hallo", COUNT);
		MyOutputClass myRunnable2 = 	new MyOutputClass("Meti", COUNT);		
		
		//nun alle gew�nschten Threads anlegen:
		MyInputClass myThread1 	= new MyInputClass();
		Thread myThread2 		= new Thread(myRunnable1);
		Thread myThread3 		= new Thread(myRunnable2);
		
		//nun alle Thread-Objekte starten
		myThread1.start();
		myThread2.start();
		myThread3.start();
	}
}


