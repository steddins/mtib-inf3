package MyThreads;
/** Der Thread gibt nach Ablauf einer Z�hlschleife den Text aus, mit dem er
 * initialisiert wurde.
 * @author stedS
 */
public class MyOutputClass implements Runnable {
	private String text;
	private final int COUNT;
	
	MyOutputClass(String text, int count)
	{
		this.text = text;
		COUNT = count;
	}
	
	public void run() {
		long kk = 0;
		int ll = COUNT;
		while (true) {
			if (kk++ % 20_000_000 == 0) {
				if (ll != 0) {					
					System.out.print(text + ll + " ");
					ll--;
				} else {
					System.out.println(text + ll + "newline ");
					ll=COUNT;
				}
			}
		}		
	}
}




