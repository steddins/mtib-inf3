package inf3;
/** Programm zur Demonstration von zeitgleich laufenden Threads. 
 * 
 * <ul>
 * 	<li> Das Programm liest von der Konsole Text ein (Thread1) und gibt �ber zwei
 *       weitere Threads unterschiedliche Texte auf der Konsole aus (Thread2 und 3)
 *       </li>
 *  <li> Man erkennt, dass das OS bzw. die JVM die Ausf�hrung der Threads koordiniert
 *       und dabei keine spezielle Reihenfolge beachtet. Dies ist m�glicherweise auf 
 *       entsprechende Optimierungsstrategien zur�ckzuf�hren.
 *       </li> 
 *  <li> Es soll �ber den Taskmanager beobachtet werden, wieviele Threads und Prozesse
 *       vor, w�hrend und nach dem Aufruf des Programmes angezeigt werden und wie die 
 *       Belastung der CPU ausf�llt.
 *       </li>        
 * </ul>
 * @version 2.0 (2020-01-06) Ausgabe ver�ndert
 * @version 1.0 (2016-01-11) 
 */
public class ThreadDurchVererbung_1 {
	/** Der main-Thread startet 3 unterschiedliche Threads und wird dann beendet	 
	 * @param args Keine Auswertung von Runtimeparametern
	 * @author stedS
	 */
	public static void main(String[] args) {
		final int COUNT = 5;
		MyInputClass myThread1 = new MyInputClass();
		MyOutputClass myThread2 = new MyOutputClass("Hallo", COUNT);
		MyOutputClass myThread3 = new MyOutputClass("Meti", COUNT);
		myThread1.start();
		myThread2.start();
		myThread3.start();
	}
}


