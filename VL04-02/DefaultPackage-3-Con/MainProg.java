 /** MainProg.java
 * Programm zeigt Nutzung des Default Packages in Verbindung
 * mit hierarchisch strukturierten Packages.
 * @author Steddin
 * @version 1.00, 2019-11-04
 */
public class MainProg {
	public static void main(String[] args) {
		hochschule.vorlesung.BlaBla blabla1 = new hochschule.vorlesung.BlaBla();
		hochschule.mensa.BlaBla blabla2 = new hochschule.mensa.BlaBla();
		kaffeeklatsch.BlaBla blabla3 = new kaffeeklatsch.BlaBla();
		blabla1.sayBla();
		blabla2.sayBla();
		blabla3.sayBla();
	}
}




