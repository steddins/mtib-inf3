javac -d bin --module-source-path src src\de.meti.inf3vl.modtest_main\*.java src\de.meti.inf3vl.modtest_main\de\meti\inf3vl\modtest_main\*.java

java --module-path bin -m de.meti.inf3vl.modtest_main/de.meti.inf3vl.modtest_main.mainclass


jar --create --file modules/de.meti.inf3vl.modtest_main-1.0.jar --module-version 1.0 --main-class de.meti.inf3vl.modtest_main.mainclass -C bin/de.meti.inf3vl.modtest_main .
jar --create --file modules/de.meti.inf3vl.modtest_lib-1.0.jar --module-version 1.0 -C bin/de.meti.inf3vl.modtest_lib .

jLink -p "%JAVA_HOME%\jmods;modules" --add-modules de.meti.inf3vl.modtest_main --output deployment --strip-debug --compress=2 –-verbose

jLink -p "%JAVA_HOME%\jmods;modules" --add-modules de.meti.inf3vl.modtest_main --launcher module_con=de.meti.inf3vl.modtest_main/de.meti.inf3vl.modtest_main.mainclass --output deployment --strip-debug --compress=2 –-verbose

java -m de.meti.inf3vl.modtest_main
