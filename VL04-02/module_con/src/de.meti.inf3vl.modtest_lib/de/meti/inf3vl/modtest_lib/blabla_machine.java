package de.meti.inf3vl.modtest_lib;
import static java.lang.System.out;
public class blabla_machine {
	private static blabla_machine bm = null;
	private blabla_machine() {
		//Konstruktor private --> factory Methode erforderlich
	}	
	public static blabla_machine blabla_machine_factory() {
		if (bm == null) {
			bm = new blabla_machine();
		}
		return bm;
	}
	public void doBlabla(String nonsens) {
		out.println("can't stop my " + nonsens + " blabla");
	}
}
