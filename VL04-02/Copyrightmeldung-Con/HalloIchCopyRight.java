/** HalloIchCopyRight.java
 *
 * Das Programm soll außer der Begrüßungsmeldung (Hallo meti) noch einen 
 * Copyright Verweis ausgeben, der auch in anderen Programmen zugänglich sein
 * soll. Die für die Ausgabe zuständige Klasse soll daher in einem Paket
 * hinterlegt werden, welches auch anderen Projekten zugänglich sein soll.
 *  
 * Die Klasse Copyrightmeldung.class wird gemäß der package-Anweisung in der 
 * Quelldatei im Paket de.hsreutlingen.inf.metipack.inf3
 * angelegt und wird beim Programmaufruf im Dateisystem in der zugehörigen 
 * Verzeichnisstruktur gesucht. Die Wurzel dieser Verzeichnisstruktur ist 
 * entweder 
 * a) im aktuellen Verzeichnis, von dem aus die JVM aufgerufen wird oder
 *  b) in einem der unter der Umgebungsvariablen CLASSPATH hinterlegten Verzeichnisse
 *  
 * Beispiel:
 * Wird das Programm aus dem Verzeichnis c:\JavaProgs gestartet, so wird die
 * Klasse Copyrightmeldung im Verzeichnis c:\JavaProgs\de\hsreutlingen\inf\metipack\inf3
 * erwartet. Bei der Programminstallation muss das entsprechende Verzeichnis angelegt werden.
 *  
 *
 * Die Sourcecode-Dateien müssen gemäß der in der import Direktive festgelegten Verzeichnis-
 * struktur abgelegt sein. Anderfalls kann der Compiler die Sourcedatei nicht finden und die
 * Übersetzung von HalloIchCopyright bricht ab, da das Import-Verzeichnis nicht auffindbar ist.
 *  
 * Ist die Übersetzung erfolgreich, so wird die .class Datei im gleichen Verzeichnis abgelegt,
 * wie die Quelldatei. Auf diese Weise ist gewährleistet, dass beim Aufruf des Programmes 
 * die .class-Datei im über den package Namen festgelegten Verzeichnisbaum liegt.
 *  
 * Für die Übersetzung von HalloIch müssen die Sourcedateien von Copyrightmeldung und 
 * Infomeldung nicht mehr vorliegen, wenn deren .class-Dateien im zugehörigen package-
 * Verzeichnis liegen.
 *
 * @author Steddin
 * @version 1.00, 2016-11-18
 */  
import de.hsreutlingen.inf.metipack.inf3.Copyrightmeldung;

public class HalloIchCopyright {

	public static void main(String[] args) {
		System.out.println("Hallo meti");
		Copyrightmeldung cm = new Copyrightmeldung();
		cm.ZeigeCopyright();
	}
}
