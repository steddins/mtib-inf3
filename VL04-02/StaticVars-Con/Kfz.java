/** Kfz.java
 * 
 * Verwendung von statischen Variablen und Methoden:
 * Das Programm enth�lt Fehler, die korrigiert werden sollen.
 *
 * @author Steddin
 * @version 1.00, 2016-11-18
 */

public class Kfz {
    int anzSitze = 2;
	int anzRaeder = 2;	

	public static void main(String[] args) {	
		Fahrrad fahrrad = new Fahrrad();
		System.out.println("Kfz: Anzahl Autositze: "+ GetAnzSitze());
		System.out.println("Fahrrad: Anzahl R�der: "+ fahrrad.GetAnzRaeder());
	}
	
	int GetAnzSitze() {
		return anzSitze;
	}
}

public class Fahrrad {
	int GetAnzRaeder() {
		return Kfz.anzRaeder;
	}
}
