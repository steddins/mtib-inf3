 /** MainProg.java
 * Programm zeigt Nutzung des Default Packages zusammen mit 
 * untergeordneten Packages.
 * @author Steddin
 * @version 1.00, 2019-11-04
 */
public class MainProg {
	public static void main(String[] args) {
		vorlesung.BlaBla blabla1 = new vorlesung.BlaBla();
		kaffeeklatsch.BlaBla blabla2 = new kaffeeklatsch.BlaBla();
		blabla1.sayBla();
		blabla2.sayBla();
	}
}

