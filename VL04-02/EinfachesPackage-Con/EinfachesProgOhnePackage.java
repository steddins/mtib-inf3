/** VL04-02_EinfachesProgOhnePackage
 * Ohne die Angabe eines Packages muss die Datei zur Übersetzung und zur Ausführung in dem 
 * Verzeichnis liegen, in dem die Konsole der JVM gestartet wird.
 * @author Steddin
 * @version 1.00, 2016-11-18
 */
public class EinfachesPackage {

	public static void main(String[] args) {	
		System.out.println("Hallo metis :-)");
	}
}