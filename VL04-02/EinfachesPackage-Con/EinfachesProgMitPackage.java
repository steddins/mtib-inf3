/** VL04-02_EinfachesProgMitPackage
 * Ohne die Angabe eines Packages muss die Datei zur Übersetzung und zur Ausführung in dem 
 * Verzeichnis liegen, in dem die Konsole der JVM gestartet wird.
 * Mit Angabe des Packages muss das Programm aus dem über dem Package-Verzeichnis liegenden
 * Verzeichnis heraus aufgerufen werden; hierbei muss im Aufruf der Pfad angegeben werden. 
 * @author Steddin
 * @version 1.00, 2019-11-04
 */
package mypackage;
public class EinfachesProgMitPackage {

	public static void main(String[] args) {	
		System.out.println("Hallo metis :-)");
	}
}