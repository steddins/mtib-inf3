 /** MainProg.java
 * Programm zeigt Nutzung des Default Packages.
 * @author Steddin
 * @version 1.00, 2019-11-04
 */
 public class MainProg {
	public static void main(String[] args) {
		BlaBla blabla = new BlaBla();
		blabla.sayBla();
	}
}

