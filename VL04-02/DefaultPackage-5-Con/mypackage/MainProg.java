 /** MainProg.java
 * Programm zeigt Nutzung der import Direktive:
 * Durch die Nutzung identischer Klassennamen in 
 * den unterschiedlichen Packages ist die Verwendung 
 * der import-Direktive nur eingeschränkt sinnvoll. 
 * @author Steddin
 * @version 1.00, 2019-11-04
 */
package mypackage;
import hochschule.vorlesung.*;
import kaffeeklatsch.BlaBla;

public class MainProg {
	public static void main(String[] args) {
		BlaBla blabla1 = new BlaBla();
		hochschule.mensa.BlaBla blabla2 = new hochschule.mensa.BlaBla();
		BlaBla blabla3 = new BlaBla();
		blabla1.sayBla();
		blabla2.sayBla();
		blabla3.sayBla();
	}
}




