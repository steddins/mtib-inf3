 /** MainProg.java
 * Programm zeigt vollständige Nutzung des Package Konzepts.
 * Aufgabe für Vorlesung: Programm soll über Kommandozeile
 * übersetzt und gestartet werden. 
 * @author Steddin
 * @version 1.00, 2019-11-04
 */
package mypackage;
public class MainProg {
	public static void main(String[] args) {
		hochschule.vorlesung.BlaBla blabla1 = new hochschule.vorlesung.BlaBla();
		hochschule.mensa.BlaBla blabla2 = new hochschule.mensa.BlaBla();
		kaffeeklatsch.BlaBla blabla3 = new kaffeeklatsch.BlaBla();
		blabla1.sayBla();
		blabla2.sayBla();
		blabla3.sayBla();
	}
}




