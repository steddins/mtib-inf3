/** MyStaticPi.java
 * 
 * Das Projekt demonstriert, wie der statische Import von
 * Konstanten und von Methoden erfolgen kann.
 *
 * @author Steddin
 * @version 2.00, 2019-11-04
 */ 

package mypackage;
//f�r Zugriff auf statische Elemente der Klasse myMath.Misc erforderlich:
//import static de.hsreutlingen.inf.metipack.inf3.myMath.Misc.*;
//f�r Zugriff auf nicht statische Elemente (Methoden) der Klasse myMath.Misc erforderlich:
import de.hsreutlingen.inf.metipack.inf3.myMath.Misc;

public class MyStaticPI {

	public static void main(String[] args) {
		float erg;
		float radius = 2.0f;
		//Ohne statischen Import m�sste folgender Befehl verwendet werden
		erg = 2.0f * radius * de.hsreutlingen.inf.metipack.inf3.myMath.Misc.PI_F;
		//erg = 2.0f * radius * PI_F;
		System.out.println("Umfang(Radius r = " + radius + " cm) = " + erg + " cm");
		System.out.println("Flaeche = " + de.hsreutlingen.inf.metipack.inf3.myMath.Misc.GetKreisFlaeche(radius) + "cm�");
		//System.out.println("Flaeche = " + GetKreisFlaeche(radius) + "cm�");
		//nicht statische Methoden k�nnen nur nach Erzeugung einer Instanz 
		//der Klasse verwendet werden:
		Misc helper = new Misc();		
		System.out.println("Kugelvolumen = " + helper.GetKugelVolumen(radius));
		//Aufruf �ber anonyme Klasse:
		System.out.println("Kugelvolumen = " + new Misc().GetKugelVolumen(radius));		
	}
}


