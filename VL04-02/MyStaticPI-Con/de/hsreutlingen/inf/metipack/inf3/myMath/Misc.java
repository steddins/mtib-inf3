/** Misc.java
 * 
 * Anhand der Klasse Misc soll demonstriert werden, wie man sich eine
 * eigene Bibliothek mit häufig verwendeten Funktionen und Konstanten 
 * in einem Package anlegen kann. <p>
 * Das Package heißt myMath und könnte außer der Klasse Misc auch noch 
 * andere Klassen aufnehmen, welche Mathematik-Funktionen anbieten würden.
 *
 * @author Steddin
 * @version 1.00, 2016-11-18
 */ 
package de.hsreutlingen.inf.metipack.inf3.myMath;

public class Misc {
	public static final float PI_F = 3.14f;
	public static final int REGEL_ANZ_METI_SEMESTER = 7;
	
	public static float GetKreisFlaeche(float radius) {
		return (radius * radius * PI_F);
	}
	public double GetKugelVolumen(double radius) {
		return (4.0 * PI_F * Math.pow(radius,3));
	}	
}


